<p align="center">
  <a href="https://gitlab.com/odrassam/react-space-chatting">
    <img src="https://drive.google.com/file/d/1evQRNr0eu-vf34gtgpu7Py04W9Jn5x4K/view?usp=sharing" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Space Chat</h3>

  <p align="center">
    <a href="https://rdh-space-chat.netlify.app/">View Demo</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
Spacechatting is a React-Redux application where you can meet socialize with your friends.

### Built With
This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.

* ReactJs
* SCSS
* Firebase
* Redux
* Material UI


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/odrassam/react-space-chatting
   ```
2. Install NPM packages
   ```sh
   npm install
   ```






